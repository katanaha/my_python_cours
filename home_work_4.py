# Andrew Katanakha | lesson 4 

information_about_myself = {
	'first_name': 'Andrew',
	'second_name': 'Katanakha',
	'age': 27,
	'height': '193 sm',
	'job': 'business analyst',
	'city': 'Khmelnitskiy',
	'nationality': 'Ukrainian',
	'skills': '1C, sql, english'
}

marks_of_auto = ['BMV', 'Mercedes-Benz', 'Audi', 'Opel', 'Peugeot', 'Renault', 'Toyota', 'Nissan', 'Lexus', 'Ford', 'Tesla', 'Lada',
'Ferarri', 'Fiat', 'Skoda','Volvo', 'Mitsubishi ', 'Kia', 'Jeep', 'Jaguar', 'Honda']
marks_of_auto_dict = {}
key = range(len(marks_of_auto))
for auto in key:
	marks_of_auto_dict[auto + 1] = marks_of_auto[auto]
print(marks_of_auto_dict)

the_best_national_cars = {}
the_best_national_cars.update({input('enter the country '): input('enter the best mark in this country ')})
next_car = input('Do you want continue? ')
while next_car.lower() == 'yes':
    the_best_national_cars.update({input('enter the country '): input('enter the best mark in this country ')})
    next_car = input('Do you want continue? ')
print(the_best_national_cars)

capital_of_countries = {
    'Kiev': 'Ukraine',
    'London': 'UK',
    'Paris': 'France',
    'Berlin': 'Germany',
    'Madrid': 'Spain',
    'Rome': 'Italy',
    'Tokio': 'Japane',
    'New Deli': 'India',
    'Kair': 'Egypt',
    'Moskow': 'Russia',
    'Minsk': 'Belorussia'
}
try:
    poped_capital = capital_of_countries.pop(input('Please, enter the name of capital: '))
    print(capital_of_countries)
except KeyError as message:
    print('You wrote uncorrect value of capital!')
    print('Do you want add your capital to dict?')
    user_decisions = input()
    if user_decisions.lower() == 'yes':
    	capital_of_countries.update({input('Please, repeat namo of capital: '): input('Please, enter the country: ')})
print(capital_of_countries)

presidents_of_Ukraine_with_ending_cadence = ['Kravchuk', 'Kuchma', 'Yuchenko', 'Yanukovich', 'Poroshenko']
years_of_presidents_of_Ukraine_with_ending_cadence = ['1991 - 1994', '1994 - 2005', '2005 - 2010', '2010 - 2014', '2014 - 2019', '2019 - for now']
cadence = 0
while True:
    try:
        print('President of Ukraine in: ', years_of_presidents_of_Ukraine_with_ending_cadence[cadence],
              presidents_of_Ukraine_with_ending_cadence[cadence])
        cadence += 1
    except IndexError as message:
        print(years_of_presidents_of_Ukraine_with_ending_cadence[cadence], '- Zelensky is still president')
        break

dollar = 27.5
workers_salary_uah = {
    'Anton': 9000,
    'Pavlo': 8090,
    'Sergey': 9500,
    'Dima': 12000,
    'Ann': 7800,
    'Kate': 13700,
    'Oleg': "he doesn't work yet"
}
for key in workers_salary_uah:
    try:
        print(key, 'has on dollars ', workers_salary_uah[key] / dollar)
    except TypeError as message:
        print(key, "doesn't has sallary yet")

the_biggest_river_in_the_country = {
    'Ukraine': 'Dnipro',
    'Russian': 'Volga',
    'Romania': 'Danube'
}
while True:
    print('Commands menu:')
    print('0. View dictionary item')
    print('1. Add a new river')
    print('2. Delete the dictionary item')
    print('3. Edit dictionary item')
    print("4. Delete the dictionary")
    choosen_command = int(input('Enter the commands number: '))
    user_answer = '0'
    if choosen_command == 0:
        print('View dictionary item')
        for key in the_biggest_river_in_the_country:
            print(key)
        print(the_biggest_river_in_the_country.get(input('Enter name of country from the list: ')))
        user_answer = input('If you want continue => press "yes": ')
    if choosen_command == 1:
        print('Add a new item ')
        the_biggest_river_in_the_country[input('Enter country: ')] = input('Enter name of river: ')
        user_answer = input('If you want continue => press "yes": ')
    if choosen_command == 2:
        print('Delete the dictionary item')
        deleted_elements = input('Enter name of country: ')
        the_biggest_river_in_the_country.pop(deleted_elements)
        print(the_biggest_river_in_the_country)
        user_answer = input('If you want continue => press "yes": ')
    if choosen_command == 3:
        print('Edit dictionary item')
        the_biggest_river_in_the_country.update({input('Enter name of country: '): input('Enter name of river: ')})
        print(the_biggest_river_in_the_country)
        user_answer = input('If you want continue => press "yes": ')
    if choosen_command == 4:
        del(the_biggest_river_in_the_country)
        print('Dictionary has been deleted. Buy!')
        break
    if choosen_command < 1 and choosen_command > 4:
        print('You have chosen incorrect commands, please try again!!!')
    if user_answer.lower() != 'yes':
        print('Thank you for visit! BUY!!!')
        break